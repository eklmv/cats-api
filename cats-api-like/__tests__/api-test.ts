import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';

const cats: CatMinInfo[] = [{ name: 'Суперавтотестер', description: '', gender: 'male' }];

let catId;

const fakeId = 'fakeId';

const HttpClient = Client.getInstance();

describe('API лайков', () => {
  beforeAll(async () => {
    try {
      const add_cat_response = await HttpClient.post('core/cats/add', {
        responseType: 'json',
        json: { cats },
      });
      if ((add_cat_response.body as CatsList).cats[0].id) {
        catId = (add_cat_response.body as CatsList).cats[0].id;
      } else throw new Error('Не получилось получить id тестового котика!');
    } catch (error) {
      throw new Error('Не удалось создать котика для автотестов!');
    }
  });

  afterAll(async () => {
    await HttpClient.delete(`core/cats/${catId}/remove`, {
      responseType: 'json',
    });
  });

  it('Поставить лайк котику', async () => {
    const response = await HttpClient.post(`likes/cats/${catId}/likes`, {
      responseType: 'json',
      json: {
        like: true,
      },
    });
    expect(response.statusCode).toEqual(200);

    expect(response.body).toEqual({
      id: catId,
      ...cats[0],
      tags: null,
      likes: 1,
      dislikes: 0,
    });
  });

  it('Поставить дизлайк котику', async () => {
    const response = await HttpClient.post(`likes/cats/${catId}/likes`, {
      responseType: 'json',
      json: {
        dislike: true,
      },
    });
    expect(response.statusCode).toEqual(200);

    expect(response.body).toMatchObject({
      id: catId,
      ...cats[0],
      tags: null,
      dislikes: 1,
    });
  });

  it('Попытка поставить лайк с некорректным id вызовет ошибку', async () => {
    await expect(
      HttpClient.post(`likes/cats/${fakeId}/likes`, {
        responseType: 'json',
        json: {
          like: true,
        },
      })
    ).rejects.toThrowError('Response code 400 (Bad Request)');
  });

  it('Метод получения рейтинга котов', async () => {
    const response = await HttpClient.get('likes/cats/rating', {
      responseType: 'json',
    });
    expect(response.statusCode).toEqual(200);

    expect(response.body).toEqual({
      likes: expect.arrayContaining([
        expect.objectContaining({
          id: expect.any(Number),
          name: expect.any(String)
        }),
      ]),
      dislikes: expect.arrayContaining([
        expect.objectContaining({
          name: expect.any(String),
          dislikes: expect.any(Number),
        }),
      ]),
    });
  });
});
