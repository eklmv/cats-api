import Client from '../dev/http-client';
import type { CatMinInfo, Cat, CatsList } from '../dev/types';

export const emptyMaleCatMinInfo: CatMinInfo = { name: null, description: '', gender: 'male' };
export const emptyMaleCat: Cat = {
  ...emptyMaleCatMinInfo,
  id: null,
  tags: null,
  likes: 0,
  dislikes: 0,
};

export function randomName(length: number) {
  const chars = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя';
  let name = '';

  for (let i = 0; i < length; i++) {
    name += chars.charAt(Math.floor(Math.random() * chars.length));
    if (i == 0) {
      name = name.toUpperCase();
    }
  }
  return name;
}

export async function randomMaleCat(attempts = 5, nameLen = 30): Promise<[number, string]> {
  const HTTPClient = Client.getInstance();
  let catID: number;
  let catName: string;
  for (let i = 0; i < attempts; i++) {
    const newName = randomName(nameLen);
    const cats: CatMinInfo[] = [{ ...emptyMaleCatMinInfo, name: newName }];
    const response = await HTTPClient.post('core/cats/add', {
      responseType: 'json',
      json: { cats },
    });
    if ((response.body as CatsList).cats[0].id) {
      catID = (response.body as CatsList).cats[0].id;
      catName = newName;
      return [catID, catName];
    }
  }
  if (!catID) {
    throw new Error('не смогли получить id тестового котика');
  }
}
