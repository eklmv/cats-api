import Client from '../../dev/http-client';
import 'jest-extended';

const HTTPClient = Client.getInstance();

describe('тесты для allByLetter', () => {
  it('Получение списка котов сгруппированных по группам', async () => {
    const response = await HTTPClient.get('core/cats/allByLetter', {
      responseType: 'json',
    });

    expect(response.statusCode).toEqual(200);

    // За эталон взят фактический ответ api
    expect(response.body).toEqual({
      groups: expect.arrayContaining([
        expect.objectContaining({
          title: expect.any(String),
          cats: expect.arrayContaining([
            expect.objectContaining({
              id: expect.any(Number),
              name: expect.any(String),
              description: expect.toBeOneOf([expect.any(String), null]),
              tags: expect.toBeOneOf([expect.any(String), null]),
              gender: expect.toBeOneOf(['male', 'female', 'unisex']),
              likes: expect.any(Number),
              dislikes: expect.any(Number),
              count_by_letter: expect.any(String),
            }),
          ]),
          count_in_group: expect.any(Number),
          count_by_letter: expect.any(Number),
        }),
      ]),
      count_output: expect.any(Number),
      count_all: expect.any(Number),
    });
  });
});
