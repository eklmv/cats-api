import Client from '../../dev/http-client';
import type { Cat } from '../../dev/types';
import * as utils from '../../utils/utils';

let catID: number;
let catName: string;

const HTTPClient = Client.getInstance();

describe('тесты для save-description', () => {
  beforeAll(async () => {
    try {
      [catID, catName] = await utils.randomMaleCat();
    } catch (error) {
      throw new Error(`не удалось создать котика для автотестов: ${error}`);
    }
  });

  afterAll(async () => {
    try {
      const response = await HTTPClient.delete(`core/cats/${catID}/remove`, {
        responseType: 'json',
      });
    } catch (error) {
      // workaround чтобы хотя бы залогировать исключение, afterAll их съедает в данной версии jest
      // в jest-circus этот баг пофикшен
      console.log(`не удалось удалить тестового котика: ${error}`);
      throw error;
    }
  });

  it('Добавление описания котику', async () => {
    const descMinLen = 10;
    const descMaxLen = 1000;
    const description = utils.randomName(
      Math.floor(Math.random() * (descMaxLen - descMinLen)) + descMinLen
    );

    const expected: Cat = {
      ...utils.emptyMaleCat,
      id: catID,
      name: catName,
      description: description,
    };

    const response = await HTTPClient.post('core/cats/save-description', {
      responseType: 'json',
      json: {
        catId: catID,
        catDescription: description,
      },
    });

    expect(response.statusCode).toEqual(200);
    expect(response.body).toEqual(expected);
  });
});
