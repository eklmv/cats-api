import Client from '../../dev/http-client';
import type { Cat } from '../../dev/types';
import * as utils from '../../utils/utils';

let catID: number;
let catName: string;

const HTTPClient = Client.getInstance();

describe('тесты для get-by-id', () => {
  beforeAll(async () => {
    try {
      [catID, catName] = await utils.randomMaleCat();
    } catch (error) {
      throw new Error(`не удалось создать котика для автотестов: ${error}`);
    }
  });

  afterAll(async () => {
    try {
      const response = await HTTPClient.delete(`core/cats/${catID}/remove`, {
        responseType: 'json',
      });
    } catch (error) {
      // workaround чтобы хотя бы залогировать исключение, afterAll их съедает в данной версии jest
      // в jest-circus этот баг пофикшен
      console.log(`не удалось удалить тестового котика: ${error}`);
      throw error;
    }
  });

  it('Поиск существующего котика по id', async () => {
    const expected: Cat = {
      ...utils.emptyMaleCat,
      id: catID,
      name: catName,
    };

    const response = await HTTPClient.get('core/cats/get-by-id', {
      responseType: 'json',
      searchParams: {
        id: catID,
      },
    });

    expect(response.statusCode).toEqual(200);
    expect(response.body).toEqual({ cat: expected });
  });

  it('Поиск котика по невалидному id', async () => {
    const invalidID = 'invalidID';
    const expected = 'Response code 400 (Bad Request)';

    await expect(
      HTTPClient.get('core/cats/get-by-id', {
        responseType: 'json',
        searchParams: {
          id: invalidID,
        },
      })
    ).rejects.toThrowError(expected);
  });
});
