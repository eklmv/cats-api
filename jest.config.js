module.exports = {
  testMatch: ['<rootDir>/**/__tests__/**/*.ts'],
  transform: {
    '^.+\\.(ts)$': ['ts-jest', { tsconfig: '<rootDir>/tsconfig.json' }],
  },
  verbose: true,
  setupFilesAfterEnv: ['jest-allure/dist/setup', 'jest-extended/all'],
};
